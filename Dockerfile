FROM debian:buster-slim
RUN apt-get update && \
    env DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    wget \
    libnss3 \
    ca-certificates \
    libasound2 \
    pulseaudio-utils
RUN wget 'https://d11yldzmag5yn.cloudfront.net/prod/3.5.383291.0407/zoom_amd64.deb' \
    && apt install -y --no-install-recommends ./zoom_amd64.deb \
    && rm -Rf /var/lib/apt/lists/* \
    && apt-get purge -y --auto-remove wget

CMD [ "zoom" ]
